// Define libraries
//-----------------------------
#include <WiFi.h>
#include <DHTesp.h>  // Library to be used to read the DHT22 sensor
#include <PubSubClient.h>
#include "calculations.h"
#include "classSensor.h"

#define DHT_PIN 23
//-----------------------------
//-----------------------------d

// MQTT connection parameters
//------
const char* MQTT_USER = "user5"; // *** Replace with your username, e.g. user2, user3 etc. (leave it as it is for the test)
const char* MQTT_CLIENT = "user5";  // *** Must be unique, replace with your username, e.g. user2, user3 etc. (same as MQTT_USER)
const char* MQTT_ADDRESS = "esp-32.zapto.org";
//------
//------

// WiFi connection parameters (WPA security protocol) 
// *** Replace with your onw SSID name and password
//------
const char* WIFI_SSID     = "COSMOTE-040B82";
const char* WIFI_PASSWORD = "N2FtSUuvHavkETxh";
//------
//------

char topicControl[150], topicData[150],topicDataCrc[150];
char crcDataMessage[10];

unsigned long previous_timestamp=0;
bool startComputation=false;  //na 3ekinaei upologismoys afou ftiaxei ta oria
bool errorPubBounds=false;    //error publishing message for bounds
bool errorPubCrcData=false;   //error publishing message for crc data

void setVariables(String configurationCode);

void callback(char*, byte*, unsigned int);

Sensor sensor;


WiFiClient wifiClient;
PubSubClient client(MQTT_ADDRESS, 1883, callback, wifiClient); //server,port,callback,network client to use


void callback(char* topic, byte* payload, unsigned int length) {
  char message[length + 1]; //gia na xwraei termatikos xarakthras
  memcpy(message, payload, length);
  message[length] = '\0'; //termatikos xarakthras

  // Print received messages and topics
  //------
  Serial.println("");
  Serial.println("Callback function");
  Serial.println("Topic: ");
  Serial.println(topic);
  Serial.println("Message: ");
  Serial.println(message);

  // Check received messages and topics
  //------
  if (strcmp(topic,topicData)==0) {
    topic_Data_callback(message);
  }
  else if (strcmp(topic,topicControl)==0) {
    topic_Control_callback(message);
  }
}

void topic_Control_callback(char *message){
  if(strcmp(message,"updateConfig")==0){
    Serial.println("broker send updateConfig, stamatw deigmatolhpsia mexri na parw nea oria");
    startComputation=false; //stamataw deigmatolhpsia mexri na parw to nea oria
    sensor.clean_array(); //arxikopoihsh pinaka metrhsewn
    sendMessageBounds();  //publish gia na steilw sendConfig
    client.loop(); 
  }
  else if(strcmp(message,"startMeasurements")==0){
    startComputation=true;
  }
  else if(strcmp(message,"crcError")==0){
    Serial.println("error at crc");
    Serial.println("Let's ask again");
    sendMessageBounds();  //publish gia na steilw sendConfig
    client.loop();
  }
}

void topic_Data_callback(char *message){ 
  Serial.println("o server elabe configuration, apanthse " + String(message));
  setVariables(message);
  set_Crc_message();
  client.disconnect();    //testing gia errors, tsekarw loop gia na 3anastelnei mhnuma
  sendMessageCrcData();  //publish crcdata
  client.loop();
}

void set_Crc_message(){
  unsigned int _delay,_temp,_hum;
  _delay=Compute_CRC8_bounds(sensor.get_delay());    //kalw ton upologismo tou crc kai ftiaxnw to mhnyma dataCrc
  _temp=Compute_CRC8_bounds(sensor.get_temperature());
  _hum=Compute_CRC8_bounds(sensor.get_humidity());
  crcDataMessage[0]='\0';
  snprintf(crcDataMessage,10,"%03u%03u%03u",_delay,_temp,_hum);
}

void setVariables(char *message){    //ftiaxnw ta oria kathe fora pou ta pairnw, upologizw crc kai stelnw crcData
  Serial.println("setvariables"+String(message));
  char temp[3];
  temp[2]= '\0';
  temp[0]=message[0];
  temp[1]=message[1];
  sensor.set_delay_bound(atoi(temp));
  Serial.println("delay= " + sensor.get_delay());

  temp[0]=message[2];
  temp[1]=message[3];
  temp[2]= '\0';
  sensor.set_temp_bound(atoi(temp));
  Serial.println("temp= " + sensor.get_temperature());

  temp[0]=message[4];
  temp[1]=message[5];
  temp[2]= '\0';
  sensor.set_hum_bound(atoi(temp));
  Serial.println("humidity= " + sensor.get_humidity());
}

void mqttReconnect() {
  // Loop until we're reconnected
  Serial.print("Wait for MQTT broker...");
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(MQTT_CLIENT, "samos", "karlovasi33#")) { //an sundethei epituxws
      Serial.println("MQTT connected");
      topicSubscribe();
    } 
    else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void topicSubscribe() { //synarthsh gia subscribe
  if(client.connected()) {

    Serial.println("Subscribe to MQTT topics: ");
    Serial.println(topicControl);     //emfanish kai apostolh twn topics
    Serial.println(topicData);
    Serial.println(topicDataCrc);
    client.subscribe(topicControl);
    client.subscribe(topicData);
    client.subscribe(topicDataCrc);
    client.loop();
  }  
}

void sendMessageBounds(){ //stelnw sendconfig h crcData
  if (client.connected()) { // if MQTT server connected
    client.publish(topicControl, "sendConfig");
    Serial.println("");
    Serial.println("**************************");
    Serial.println("Message published");
    errorPubBounds=false;
    return;
  }
  else{
    Serial.println("publish Bounds message problem");
    errorPubBounds=true;
    return;
  }
}  

void sendMessageCrcData(){
  if (client.connected()) { // if MQTT server connected
    client.publish(topicDataCrc, crcDataMessage);
    Serial.println("");
    Serial.println("**************************");
    Serial.println("Message published");
    errorPubCrcData=false;
    return;
  }
  else{
    Serial.println("publish Crc data message problem");
    errorPubCrcData=true;
    return;
  }
}

//-----------------------------

void connect2Wifi(){
  Serial.print("Connecting to ");
  Serial.println(WIFI_SSID);
  
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("Wait for WiFi...");
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi Connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  return;
}

//-------------------------------------------------------------------------------------

void setup() {
  // put your setup code here, to run once:
  

  // Connect to WiFi and establish serial connection
  //------
  Serial.begin(115200);
  delay(1000);

  Serial.println();

  connect2Wifi(); //sundesh sto WiFi

  // Connect to MQTT broker and subscribe to topics
  //------
  client.setCallback(callback);   //arxikopoihsh gia sunarthsh pou xeirizetai ta mhnumata topic kai payload

  // Define MQTT topic names
  sprintf(topicControl, "%s/%s", MQTT_USER, "control");
  sprintf(topicData, "%s/%s", MQTT_USER, "data");
  sprintf(topicDataCrc, "%s/%s", MQTT_USER, "dataCrc");

  // Subscribe to topics and reconnect to MQTT server
  mqttReconnect();  //1h sundesh kai subscribe
  Serial.println("1h sundesh mqtt reconnect");
  
  sensor.initialize(DHT_PIN);

  sendMessageBounds();  //publish gia na parw oria
  
  client.loop();  
}

void loop() {
  // put your main code here, to run repeatedly:
  
  // Reconnect to WiFi if not connected
  if (WiFi.status() != WL_CONNECTED) {
    connect2Wifi();
  }

  // Reconnect to MQTT broker if not connected
  //------
  if (!client.connected() && errorPubBounds==false && errorPubCrcData==false) {
    mqttReconnect();
    Serial.println("1h loop sundesh mqtt reconnect");
  }
  else if(errorPubBounds && (millis()-previous_timestamp>2000)){  //an ginei lathos kanw elegxo kathe 2 deuterolepta, alliws trexei polles fores, anti gia delay()
    Serial.println("errorPubBounds sundesh mqtt reconnect");
    mqttReconnect();
    sendMessageBounds();
    previous_timestamp=millis();
  }                       
  else if(errorPubCrcData && (millis()-previous_timestamp>2000)){ 
    Serial.println("errorPubCrcData sundesh mqtt reconnect");
    mqttReconnect();
    sendMessageCrcData();         //an den einai global, tote edw pws tha thn kalw????????????
    previous_timestamp=millis();
  }
  //------
  
  if(startComputation){
    sensor.loop();
  }
  
  client.loop();
}
