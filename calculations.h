#ifndef __CALCULATIONS__
#define __CALCULATIONS__

#define INT_NUM_BYTES 4

byte Compute_CRC8(byte *array,unsigned int num_bytes)
{
  byte crc = 0;
  const byte generator=5;

  for(int i = 0; i<num_bytes; i++) //o int einai 4 bytes, ara 3ekinaw apo to aristero byte kai paw dexia gia na kanw crc
  {
    crc ^= array[i];
    for(int j = 0; j < 8 ; j++)
    {
      if((crc & 0x80) != 0)
      {
        crc = (byte)((crc << 1) ^ generator);
      }
      else
      {
        crc <<= 1;
      }
    }
  }
  return crc;
}

unsigned int Compute_CRC8_bounds(unsigned int number) { //upologizw crc kathe oriou kai ftiaxnw to mhnyma
  byte dest[INT_NUM_BYTES];       //afou einai int ta oria, tote exw 4 bytes
  byte crc;

  for(int i=0;i<INT_NUM_BYTES;i++){
    dest[i]= (number >> 8*(INT_NUM_BYTES-1-i)) & 255;
  }
  crc = Compute_CRC8(dest,INT_NUM_BYTES);
  Serial.println("crc= " + String(crc));

  return (unsigned int) crc;
}

#endif
